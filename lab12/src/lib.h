#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**
 * Максимальна кількість строк, що може бути зчитана з файлу
 */
#define MAX_C 100

/**
 * Функція, що конвертує подану строку у число
 * @param string Строка, яку треба конвертувати
 * @return число, яке було записано у строці
 */
double str_to(const char *string);
