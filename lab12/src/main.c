/**
 * # Лабораторна робота 12
 * Строки Null-terminated C Strings
 * @author Demkiv
 * @date 
 * @version 1.0.0
 */

#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main()
{
	/* Ініціалізація змінних */
	char *stroka = massiv(MAX_C);
	double output = 0;

	/* Отримуємо ввід данних від користувача */
	printf("%s\n", "Введіть число, яке ви хочете конвертувати у тип int чи float: ");
	fgets(stroka, MAX_C, stdin);

	/* Конвертуемо строку в число */
	output = str_to(stroka);

	/* Виводимо результат роботи функції */
	printf("Результат конвертації строки: %f\n", output);

	return 0;
}
