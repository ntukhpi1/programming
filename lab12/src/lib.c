#include "lib.h"

double str_to(const char *string)
{
	double ans = 0;
	double fr = 1;
	bool point_to = false;

	for (int i = 0; (*(string + i) != '\0' && *(string + i) != '\n'); i++) {
		if (*(string + i) == '.' || *(string + i) == ',') {
			point_to = true;
			continue;
		}

		if (point_to == false) {
			ans = (ans * 10) + (*(string + i) - '0');
		} else {
			fr = fr / 10;
			ans = ans + (fr * (*(string + i) - '0'));
		}
	}

	return ans;
}
