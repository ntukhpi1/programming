#include "lib.h"

void get_from_file(char *file_name, struct KontrolWork *structure)
{
	FILE *file = fopen(file_name, "r");
	char line[BUFER];
	int current_array_element = STUDENT_THAT_ENDED_EXAM - 1;
	while ((fscanf(file, "%[^\n]", line)) != EOF) {
		fgetc(file);

		printf("{%s}\n", line);
		sscanf(line, "%d %s %d {%d %d %d} %s",
		       &(structure + current_array_element)->checked,
		       (structure + current_array_element)->last_name,
		       &(structure + current_array_element)->mark,
		       &(structure + current_array_element)
				->work_structure.amount_of_practical_exercises,
		       &(structure + current_array_element)
				->work_structure.amount_of_theoretical_questions,
		       &(structure + current_array_element)
				->work_structure.amount_of_open_exercises,
		       (structure + current_array_element)->work_subject);

		if (structure[current_array_element].mark > MAX_MARK) {
			printf("Оцінка у студента більше ніж 100! Корупція!\n");
			exit(1);
		}

		current_array_element--;
	}
	fclose(file);
}

void put_in_file(char *file_name, struct KontrolWork *structure)
{
	FILE *output_file = fopen(file_name, "a+");
	for (int i = 0; i < STUDENT_THAT_ENDED_EXAM; i++) {
		fprintf(output_file, "{%d %s %d {%d %d %d} %s}\n",
			(structure + i)->checked, (structure + i)->last_name,
			(structure + i)->mark,
			(structure + i)
				->work_structure.amount_of_practical_exercises,
			(structure + i)
				->work_structure.amount_of_theoretical_questions,
			(structure + i)->work_structure.amount_of_open_exercises,
			(structure + i)->work_subject);
	}
	fclose(output_file);
}

struct KontrolWork *sort_works(struct KontrolWork *structure,
			       int sorting_number)
{
	int elements_count = 0;
	for (int i = 0; i < STUDENT_THAT_ENDED_EXAM; i++) {
		if ((structure + i)
			    ->work_structure.amount_of_theoretical_questions ==
		    sorting_number) {
			elements_count++;
		}
	}

	struct KontrolWork *result =
		malloc(elements_count * sizeof(struct KontrolWork));

	int index_count = 0;
	for (int i = 0; i < STUDENT_THAT_ENDED_EXAM; i++) {
		if ((structure + i)
			    ->work_structure.amount_of_theoretical_questions ==
		    sorting_number) {
			*(result + index_count) = *(structure + i);
			index_count++;
		}
	}

	return result;
}

void print_structure(struct KontrolWork *structure)
{
	for (int i = 0; i < STUDENT_THAT_ENDED_EXAM; i++) {
		printf("{%d %s %d {%d %d %d} %s}\n", (structure + i)->checked,
		       (structure + i)->last_name, (structure + i)->mark,
		       (structure + i)
			       ->work_structure.amount_of_practical_exercises,
		       (structure + i)
			       ->work_structure.amount_of_theoretical_questions,
		       (structure + i)->work_structure.amount_of_open_exercises,
		       (structure + i)->work_subject);
	}
}
