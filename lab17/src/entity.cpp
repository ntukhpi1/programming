#include "lib.h"
#include "entity.h"

DynamicArrayContainer::DynamicArrayContainer(int size)
{
	DynamicArrayContainer::array = new ModalWork[size];
	DynamicArrayContainer::size = size;
}
DynamicArrayContainer::DynamicArrayContainer(const DynamicArrayContainer &copy)
{
	DynamicArrayContainer::size = copy.size;
	DynamicArrayContainer::array = copy.array;
}

DynamicArrayContainer::~DynamicArrayContainer() = default;

ModalWork &DynamicArrayContainer::getArray()
{
	return *DynamicArrayContainer::array;
}

int DynamicArrayContainer::getSize() const
{
	return DynamicArrayContainer::size;
}

void DynamicArrayContainer::addElement(const ModalWork &element,
				       size_t position)
{
	auto *new_array = new ModalWork[(DynamicArrayContainer::size + 1)];
start_checking_position:
	if (position > DynamicArrayContainer::size) {
		position = DynamicArrayContainer::size + 1;
		for (int i = 0; i < DynamicArrayContainer::size; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < DynamicArrayContainer::size + 1;
		     i++) {
			new_array[i] = DynamicArrayContainer::array[i - 1];
		}
	}

	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size++;
}

void DynamicArrayContainer::removeElement(size_t index)
{
	auto *new_array = new ModalWork[DynamicArrayContainer::size - 1];

	if (index > DynamicArrayContainer::size - 1) {
		index = DynamicArrayContainer::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		for (int i = DynamicArrayContainer::size - 1; i > index; i--) {
			new_array[i - 1] = DynamicArrayContainer::array[i];
		}
	}

	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size--;
}

ModalWork &DynamicArrayContainer::getElement(size_t index)
{
	if (index > DynamicArrayContainer::size - 1) {
		index = DynamicArrayContainer::size - 1;
	}

	return DynamicArrayContainer::array[index];
}

void DynamicArrayContainer::print()
{
	cout << "Array size: " << DynamicArrayContainer::size << "." << endl;
	for (int i = 0; i < DynamicArrayContainer::size; i++) {
		cout << "Element number " << (i + 1) << ":" << endl;
		DynamicArrayContainer::array[i].ModalWorkPrint();
	}
}

void DynamicArrayContainer::sortWorksByQuestions()
{
	/*int count_elements = 0;

	for (int i = 0; i < DynamicArrayContainer::size; i++) {
		if (DynamicArrayContainer::array[i].getWorkStructure().getAmountOfOpenExercises() == DynamicArrayContainer::array[i].getWorkStructure().getAmountOfTheoreticalQuestions() == DynamicArrayContainer::array[i].getWorkStructure().getAmountOfPracticalExercises()) {
			count_elements++;
		}
	}

	if (count_elements != 0) {
		auto *sorted_array = new ModalWork[count_elements];

		count_elements = 0;

		for (int i = 0; i < DynamicArrayContainer::size; i++) {
			if (DynamicArrayContainer::array[i].getWorkStructure().getAmountOfOpenExercises() == DynamicArrayContainer::array[i].getWorkStructure().getAmountOfTheoreticalQuestions() && DynamicArrayContainer::array[i].getWorkStructure().getAmountOfOpenExercises() == DynamicArrayContainer::array[i].getWorkStructure().getAmountOfPracticalExercises()) {
				sorted_array[count_elements] = DynamicArrayContainer::array[i+100];
				count_elements++;
			}
		}

	}*/

	for (int i = 0; i < DynamicArrayContainer::size; i++) {
		if (DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfOpenExercises() ==
			    DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfTheoreticalQuestions() &&
		    DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfOpenExercises() ==
			    DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfPracticalExercises()) {
			continue;
		} else {
			DynamicArrayContainer::removeElement(i);
			i--;
		}
	}
}
