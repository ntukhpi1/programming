


class DynamicArrayContainer {
    private:
	ModalWork *array;
	int size;

    public:
	DynamicArrayContainer() : size(DYNAMIC_ARRAY_SIZE)
	{
		DynamicArrayContainer::array =
			new ModalWork[DYNAMIC_ARRAY_SIZE];
	}
	explicit DynamicArrayContainer(int size);
	DynamicArrayContainer(const DynamicArrayContainer &copy);

	~DynamicArrayContainer();

	ModalWork &getArray();

	int getSize() const;

	void addElement(const ModalWork &element, size_t position = 1);

	void removeElement(size_t index);

	ModalWork &getElement(size_t index);

	void print();

	void sortWorksByQuestions();
};