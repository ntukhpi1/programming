#include "lib.h"
#include "entity.h"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    int expected = 2;

    /* Виконання операцій */
    DynamicArrayContainer worksArrayContainer(3);
    worksArrayContainer.getElement(0)
            .getWorkStructure()
            .setAmountOfOpenExercises(10);
    ModalWork *Bodya = new ModalWork();
    Bodya->setStudentLastName("Bodya");
    worksArrayContainer.addElement(*Bodya);
    worksArrayContainer.removeElement(0);
    DynamicArrayContainer new_array(worksArrayContainer);
    new_array.sortWorksByQuestions();

    ASSERT_EQ(expected, new_array.getSize());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
