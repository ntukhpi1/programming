#include "lib.h"

void get_from_file(char *file_name, struct StudentsArrayContainer *container)
{
	FILE *file = fopen(file_name, "r");
	char line[BUFER];
	int current_array_element = container->size - 1;
	while ((fscanf(file, "%[^\n]", line)) != EOF) {
		fgetc(file);

		printf("{%s}\n", line);
		sscanf(line, "%d %s %d {%d %d %d} %s",
		       &(container->array + current_array_element)->checked,
		       (container->array + current_array_element)->last_name,
		       &(container->array + current_array_element)->mark,
		       &(container->array + current_array_element)
				->work_structure.amount_of_practical_exercises,
		       &(container->array + current_array_element)
				->work_structure.amount_of_theoretical_questions,
		       &(container->array + current_array_element)
				->work_structure.amount_of_open_exercises,
		       (container->array + current_array_element)->work_subject);

		if (container->array[current_array_element].mark > MAX_MARK) {
			printf("Оцінка у студента більше ніж 100! Корупція!\n");
			exit(1);
		}

		current_array_element--;
	}
	fclose(file);
}

void put_in_file(char *file_name, struct StudentsArrayContainer *container)
{
	FILE *output_file = fopen(file_name, "a+");
	for (int i = 0; i < container->size; i++) {
		fprintf(output_file, "{%d %s %d {%d %d %d} %s}\n",
			(container->array + i)->checked,
			(container->array + i)->last_name,
			(container->array + i)->mark,
			(container->array + i)
				->work_structure.amount_of_practical_exercises,
			(container->array + i)
				->work_structure.amount_of_theoretical_questions,
			(container->array + i)
				->work_structure.amount_of_open_exercises,
			(container->array + i)->work_subject);
	}
	fclose(output_file);
}

void sort_works(struct StudentsArrayContainer *container, int sorting_number)
{
	struct StudentsArrayContainer *result = init_dynamic_array(0);

	for (int i = 0; i < container->size; i++) {
		if ((container->array + i)
			    ->work_structure.amount_of_theoretical_questions ==
		    sorting_number) {
			insert(result, i, (container->array + i));
		}
	}

	free(container->array);
	container->array = result->array;
	free(result);
}

void print_structure(struct StudentsArrayContainer *container)
{
	for (int i = 0; i < container->size; i++) {
		printf("{%d %s %d {%d %d %d} %s}\n",
		       (container->array + i)->checked,
		       (container->array + i)->last_name,
		       (container->array + i)->mark,
		       (container->array + i)
			       ->work_structure.amount_of_practical_exercises,
		       (container->array + i)
			       ->work_structure.amount_of_theoretical_questions,
		       (container->array + i)
			       ->work_structure.amount_of_open_exercises,
		       (container->array + i)->work_subject);
	}
}
